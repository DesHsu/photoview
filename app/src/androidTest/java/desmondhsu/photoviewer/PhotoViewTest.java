package desmondhsu.photoviewer;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
/**
 * Created by Des on 15-Feb-16.
 */

@RunWith(AndroidJUnit4.class)
@SmallTest
public class PhotoViewTest extends ActivityInstrumentationTestCase2<PhotoViewerActivity> {

    PhotoViewerActivity mActivity;
    static final String SAMEPLE_IMAGE_URL="https://www.google.com.au/images/branding/googleg/1x/googleg_standard_color_128dp.png";

    public PhotoViewTest() {
        super(PhotoViewerActivity.class);
    }
    @Before
    public void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        mActivity = getActivity();

        //attempt to make sure data all retrieved before tests
        int imageCount= mActivity.imageDrawables.size();
        Thread.sleep(500);
        while(imageCount!=mActivity.imageDrawables.size()){
            imageCount=mActivity.imageDrawables.size();
            Thread.sleep(500);
        }
    }

    @Test
    public void testImageUrlExtractionCountCorrect(){
        assertThat(mActivity.imageURLs.size(), is(20));
    }

    @Test
    public void testImageUrlExtraction() {
        Pattern pattern = Pattern.compile("https://farm2.staticflickr.com/[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|].jpg");
        for(int i=0;i<mActivity.imageURLs.size();i++){
            Matcher m = pattern.matcher(mActivity.imageURLs.get(i));
            assertThat(m.find(), is(true));
        }
    }


    @Test
    public void testAddImageCountCorrect() throws InterruptedException {

        int count =mActivity.imageDrawables.size();
        mActivity.getImageFromUrl(SAMEPLE_IMAGE_URL);
        Thread.sleep(1000);
        assertThat(mActivity.imageDrawables.size(), is(count + 1));
    }

    @Test
    public void testFragmentInitiatedCorrectly() throws InterruptedException {

        for(int i=0;i<mActivity.imageDrawables.size();i++){
            assertNotNull(mActivity.mImageCollectionPagerAdapter.getItem(i));
        }
    }


}
