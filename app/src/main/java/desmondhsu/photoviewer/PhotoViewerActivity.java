package desmondhsu.photoviewer;

import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class PhotoViewerActivity extends AppCompatActivity {

    static String src= "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1";
    static final int MY_PERMISSION_TO_USE_INTERNET=0;
    static int THUMBNAIL_PADDING=10;
    static ArrayList<Drawable> imageDrawables = new ArrayList<Drawable>();
    static ArrayList<ImageView> thumbnailViews = new ArrayList<ImageView>();
    static ArrayList<String> imageURLs = new ArrayList<String>();

    boolean activityStopping =false;
    ImageCollectionPagerAdapter mImageCollectionPagerAdapter;
    ViewPager mViewPager;
    android.support.v7.widget.Toolbar topToolBar;
    Button refreshButton;
    LinearLayout thumbnailBar;
    HorizontalScrollView thumbnailScroll;
    LinearLayout.LayoutParams params;

    int thumbnailSize;
    int numberOfImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);


        //Toolbar
        topToolBar=(android.support.v7.widget.Toolbar)findViewById(R.id.topToolbar);
        topToolBar.setTitle("");
        setSupportActionBar(topToolBar);


        //button
        refreshButton = (Button)findViewById(R.id.refresh);
        refreshButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                new RequestTask().execute(src);
                clearAdapter();
                ((ProgressBar) findViewById(R.id.progressBar)).setVisibility(View.VISIBLE);
            }
        });

        //LinearLayout inside scrollview
        thumbnailBar= (LinearLayout)findViewById(R.id.thumbnailBar);
        thumbnailScroll= (HorizontalScrollView)findViewById(R.id.scrollView);


        /**
         * 1. Create photo view and update when image is available
         *
         * 2. Download JSON and images (2 asyncTasks)
         */
        clearAdapter();
        createPhotoView();
        new RequestTask().execute(src);

    }

    /**To reset data...
     * 1. Delete previous data
     * 2. Update pager
     * 3. Update thumbnails
     */
    private void clearAdapter() {

        imageDrawables.clear();
        mImageCollectionPagerAdapter =
                new ImageCollectionPagerAdapter(
                        getSupportFragmentManager());
        numberOfImages=imageDrawables.size();
        thumbnailBar.removeAllViews();
        resetThumbnails();
        imageURLs.clear();
    }

    @Override
    protected void onStop(){
        super.onStop();
        activityStopping =true;
    }
    @Override
    protected void onStart(){
        super.onStart();
        activityStopping =false;
    }

    private void createPhotoView() {
        mImageCollectionPagerAdapter =
                new ImageCollectionPagerAdapter(
                        getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.destroyDrawingCache();
        mViewPager.setAdapter(mImageCollectionPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //scroll thumbnailBar to selected photo
                //final int scrollPosition=(position<=1)?0:(position>=numberOfImages-2)?numberOfImages-3:position-1;

                //thumbnailBar.post(new Runnable() {
                    //@Override
                    //public void run() {

                        //thumbnailScroll.scrollTo(scrollPosition * thumbnailSize, 0);
                    //}
                //});


                setSelection(position);


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void setSelection(int position) {

        for(int i=0;i<thumbnailViews.size();i++){
            GradientDrawable border = new GradientDrawable();
            if(i!=position) {
                border.setColor(getResources().getColor(R.color.colorPadding)); //normal background

            }else{
                //set selection
                border.setColor(getResources().getColor(R.color.colorAccent)); //selected background
            }
            thumbnailViews.get(i).setBackground(border);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_TO_USE_INTERNET: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!

                } else {

                    this.finishAffinity();
                    System.exit(0);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public class ImageCollectionPagerAdapter extends FragmentStatePagerAdapter {
        public ImageCollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new ImageDisplayFragment();
            Bundle args = new Bundle();

            args.putInt(ImageDisplayFragment.ARG_COUNT, i);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return imageDrawables.size();
        }


    }

    public static class ImageDisplayFragment extends Fragment{
        int fragmentCount;
        public static final String ARG_COUNT = "IMAGE";
        private ImageView mImageView;

        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {
            /**
             *  photo_view.xml contains only imageView
             *
             *  Display image through URL provided on imageView
             *
             */
            View rootView = inflater.inflate(
                    R.layout.photo_view, container, false);
            Bundle args = getArguments();

            ((ImageView) rootView.findViewById(R.id.imageView)).setImageDrawable(imageDrawables.get(args.getInt(ARG_COUNT)));
            return rootView;
        }

        /**
         * As per Fragment document states- for fragment to restore properly, data should be parsed through
         * and an empty constructor is required
         */
        public ImageDisplayFragment() {}

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            //to arguments should be passed through args, not at instantiation
            fragmentCount=getArguments()!=null?getArguments().getInt(ARG_COUNT):0;
        }

    }

    class RequestTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... url) {
            /**
             * Download JSON
             */
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = null;
            String responseString="";
            try {
                response = httpclient.execute(new HttpGet(url[0]));
            } catch (IOException e) {
                e.printStackTrace();
            }
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                try {
                    response.getEntity().writeTo(out);
                    responseString = out.toString();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else{
                try {
                    response.getEntity().getContent().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {

            /**
             * Extracting URLs
             */

            super.onPostExecute(result);
            try {
                JSONObject resultJson = new JSONObject(result);
                JSONArray itemList = resultJson.getJSONArray("items");
                /**
                 * For each image URL found, create a new task to download it
                 */

                for(int i=0;i<itemList.length();i++){
                    String imageURL= itemList.getJSONObject(i).getJSONObject("media").getString("m");
                    imageURLs.add(imageURL);
                    new DownloadTask().execute(imageURL);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    class DownloadTask extends AsyncTask<String, Integer, Drawable> {

        @Override
        protected Drawable doInBackground(String... url) {
            InputStream is = null;
            try {
                is = (InputStream) new URL(url[0]).getContent();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Drawable photoDrawable = Drawable.createFromStream(is, "src name");

            return photoDrawable;
        }

        @Override
        protected void onPostExecute(Drawable results) {
            /**
             * For each image downloaded
             * notify pagerAdaptor to update
             */

            if(!activityStopping) {
                ReentrantLock lock = new ReentrantLock();
                lock.lock();
                try {
                    //add image
                    imageDrawables.add(results);
                    //update adaptor
                    mImageCollectionPagerAdapter.notifyDataSetChanged();

                } finally {
                    lock.unlock();
                }
            }
            numberOfImages = imageDrawables.size();
            thumbnailViews.add(addThumbnail(numberOfImages - 1));

            ProgressBar pb= (ProgressBar) findViewById(R.id.progressBar);

            if(pb.getVisibility()==View.VISIBLE){
                //hide progress bar when image available
                ((ProgressBar) findViewById(R.id.progressBar)).setVisibility(View.INVISIBLE);
                mViewPager.setCurrentItem(0);
                setSelection(0);
            }


            }

    }

    private void resetThumbnails(){
        thumbnailViews.clear();
        for(int i=0;i<imageDrawables.size();i++){
            thumbnailViews.add(addThumbnail(i));
        }
    }

    private ImageView addThumbnail(final int position) {
        //thumbnail params
        thumbnailSize = thumbnailBar.getHeight();
        params = new LinearLayout.LayoutParams(thumbnailSize, thumbnailSize);

        //create thumbnail
        final ImageView thumbView = new ImageView(this.getApplicationContext());
        thumbView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        thumbView.setLayoutParams(params);
        thumbView.setPadding(THUMBNAIL_PADDING, THUMBNAIL_PADDING, THUMBNAIL_PADDING, THUMBNAIL_PADDING);
        thumbView.setCropToPadding(true);
        thumbView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(position);
                setSelection(position);
            }
        });
        //add background
        GradientDrawable border = new GradientDrawable();
        border.setColor(0xFFFFFFFF); //white background
        thumbView.setBackground(border);

        //add image
        thumbView.setImageDrawable(imageDrawables.get(position));

        thumbnailBar.addView(thumbView);

        if(position==0&&mViewPager!=null){
            mViewPager.setCurrentItem(0);
            setSelection(0);
        }

        return thumbView;


    }





    /**
     * Extract URL from the JSON object recieved from a URL in a specific format
     * and save to imageURLs
     * @param URL
     */
    //public void extractImageUrlFromJSON(String URL){
    //    src=URL;
    //    new RequestTask().execute(src);
    //}

    /**
     * Get image from URL, save to imageDrawables
     * @param imageURL
     */
    public void getImageFromUrl(String imageURL){
        new DownloadTask().execute(imageURL);
    }


}
