package desmondhsu.photoviewer;

import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Des on 15-Feb-16.
 */


public class PhotoViewTest {

    @Test
    public void testJsonExtractionHasCorrectCount(){
        PhotoViewerActivity photoViewerActivity = new PhotoViewerActivity();
        photoViewerActivity.extractImageUrlFromJSON("https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(20,PhotoViewerActivity.imageURLs.size());
    }

    @Test
    public void testJsonExtraction(){
        PhotoViewerActivity photoViewerActivity = new PhotoViewerActivity();
        photoViewerActivity.extractImageUrlFromJSON("https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Pattern pattern = Pattern.compile("https://farm2.staticflickr.com/[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|].jpg");
        Matcher matcher;

        for(int i=0;i<PhotoViewerActivity.imageURLs.size();i++){
            matcher = pattern.matcher(PhotoViewerActivity.imageURLs.get(i));
            Assert.assertEquals(true, matcher.find());
        }
    }
    @Test
    public void testImageGetHasCorrectCount(){
        PhotoViewerActivity photoViewerActivity = new PhotoViewerActivity();
        Assert.assertEquals(0,PhotoViewerActivity.imageURLs.size());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        photoViewerActivity.getImageFromUrl("https://farm2.staticflickr.com/1526/24402725994_15e6668f83_m.jpg");
        Assert.assertEquals(1, PhotoViewerActivity.imageDrawables.size());
    }
}
